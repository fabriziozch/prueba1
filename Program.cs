﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            int[] arreglo = new int[5];/* Recordar sintaxis de esta class*/
            while (i < 5)
            {
                Console.WriteLine("Introducir valor den el arreglo espacio [" + i + 1 + "]");
                arreglo[i] = Convert.ToInt16(Console.ReadLine());
                i++;
            }
            int[] arreglo2 = new int[] {3,5,8,12,60};
            //En la parte de arriba podemos ver las dos formas de instancias arreglos.
            int n = mayorElemento(arreglo);
            Console.WriteLine("El mayor seria " + n);
            Console.WriteLine("Esto vendria a ser una modificacion");
            Console.Read();
        }
        private static int mayorElemento(int[] arreglo2)
        {
            int may = 0;
            for(int i=0; i < 5; i++)
            {
                if (arreglo2[i] > may)
                {
                    may = arreglo2[i];
                }
            }
            return may;
        }
    }
}
